create database Purchase_Sales_Inventory_Management
go
use Purchase_Sales_Inventory_Management
go
-- 职工清单
create table StaffBill(
Staffed char(6) not null, -- 职工编号
Name nvarchar(8) not null, -- 姓名
Sex nchar(2) not null, -- 性别
Home nvarchar(4) null, -- 籍贯
)

-- 用户信息表
create table UserBill(
LoginId char(6) not null, -- 账户
PassWord char(6) not null, -- 密码
)

--商品信息清单
create table MerchBill(
MerchId char(6) not null, -- 商品编号
Proffered char(6) not null, -- 供货商号
FullName nvarchar(80) not null, -- 名称
UnitCost money not null, -- 单价
StoreNum int not null, -- 库存
Others ntext not null,  -- 备注
)

--(供货商信息表)
create table ProfferBill(
Proffered char(6) not null, -- 供货商号
FullName nvarchar(50) not null, -- 名称
ContactPerson nvarchar(8) not null, -- 联系人
Phone varchar(12) not null, -- 联系电话
Other ntext not null, -- 备注
)



--（商品进货信息表）
create table ImportBill(
ImportBillid char(6) not null,  --进货单号
MerchId char(6)not null,   -- 商品编号
Proffered char(6) not null,	-- 供货商号
ImportPrice money not null,	-- 进货价
Quantity int not null,	-- 数量
TotalMoney money not null,	-- 金额
ImoprtDate datetime not null,	-- 进货日期
Others ntext null,	-- 备注
)

-- 销售信息表
create table SaleBill(
SaleBillid char(6) not null, -- 销售单号
MerchId char(6) not null, -- 商品编号
SaleNum int not null, -- 销售数量
SaleDate datetime not null, -- 销售日期
)

-- 库存信息表
create table StoreBill(
MerchId char(6) not null, -- 商品编号
toreNum int not null, -- 库存数量
)

-- 商品退货信息表
create table ReturnApplication(
odd char(11) not null, -- 单号
Flow_chart_No_1 varchar(32) not null, -- 流水单号1
ReceiverAddress varchar(8) not null, -- 收货地址
Client varchar(8) not null, -- 客户
The_network_date datetime not null, -- 网络日期
System_Date datetime not null,-- 制单日期
Auditor varchar(8) not null,-- 审核员
Audit varchar(8) not null, -- 是否审核
MaterialDate datetime not null, -- 确定日期
Whether_or_not_sure bit not null, -- 是否确认
Note varchar(200) not null, -- 备注
marker_color varchar(11) not null,-- 标记颜色
salesman varchar(8) not null,-- 销售员
department varchar(8) not null,-- 部门
The_price_set bit not null,-- 价格设置
Flow_chart_No_2 varchar(19) not null,-- 流水单号2
The_return_time datetime not null,-- 退货时间
)
