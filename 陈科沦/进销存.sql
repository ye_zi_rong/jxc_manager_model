	create database InvoicingSystem  -- 创建进销存系统

	use InvoicingSystem

	create table AccountInfo -- 用户表
	(
		AccountId int primary key identity (1,1) not null ,
		AccountPhone varchar(20) not null, -- 用户电话号码
		AccountName varchar (20) not null , -- 用户姓名
		AccountAddress varchar (200) not null, --用户地址
		AccountBalance money not null ,-- 用户余额
	)
	create table Supplier --供应商表
	(
		SupplierId int  identity (1,1) not null ,
		GoodsName varchar (15) primary key not null, --商品名字
		SupplierPhone varchar(20) not null, --供应商电话
		SupplierName varchar (20) not null ,--供应商姓名
	)
	create table Commodity --商品表
	(
		ProductId int  primary key not null ,--商品编号
		GoodsName varchar (15) not null, --商品名字
		SupplierId int not null ,
		GoodsType varchar (30) not null ,--商品类型
		GoodsPrice money not null , -- 商品价格
	)
	create table Purchase	--采购表
	(
		PurchaseId int primary key not null,
		ProductId int not null , --商品编号
		GoodsNumber int not null ,  --商品数量
		TotalAmount money not null ,--总计金额
	)
	create table WarehouseIn --入库
	(
		productId int not null,--商品编号
		GoodsName varchar(15) not null , --商品名字
		StoreNumber int not null , -- 储存数量
		InTime smalldatetime not null,-- 存入时间
	)
	create table Warehouse --仓库储存表
	(
		productId int not null,--商品编号
		GoodsName varchar(15) not null , --商品名字
		StoreNumber int not null , -- 储存数量
	)
	create table WarehouseOut -- 出库
	(
		productId int not null,--商品编号
		GoodsName varchar(15) not null , --商品名字
		OutNumber int not null , -- 取出数量
		OutTime smalldatetime not null,-- 取出时间
	)
	create table Sales -- 销售表
	(
		ProductId int not null , --商品编号
		SalesPrise money not null , -- 销售价格
		GoodsNumber int not null ,  --商品数量
		TotalAmount money not null , -- 总计金额
	)
	create table Exchange --交易记录表
	(
		ExchangeId int primary key identity(1, 1)  not null,
		ProductId int not null , -- 商品编号
		AccountId int not null , 
	--	GoodsBuy varchar (20)  not null,-- 买入商品
	--	GoodSell varchar (20)  not null, -- 卖出商品
		BuyNumber money not null , -- 买入数量
		SellNumber money not null , -- 卖出数量
		TotalAmount money not null ,--总计金额
		ExchangeTime smalldatetime not null, -- 交易时间
	)
	create table AfterSale -- 售后表
	(
		ProductId int not null , -- 商品编号
		AccountId int  not null,--退货人Id
		ReturnReason varchar (200) not null , -- 退货理由
		ReturnTime smalldatetime not null, -- 退货时间
	)















